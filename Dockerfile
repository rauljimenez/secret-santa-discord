FROM node:16

WORKDIR /usr/src/app

COPY . .

EXPOSE 8080

RUN npm i

CMD [ "node", "secret-santa.js" ]
