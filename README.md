# Secret Santa - Discord bot

Secret santa was built to provide a double blind way for everyone to participate in secret santa including the organizer. Once the bot is in your server you can do `!santa help` for a list of commands.

## Tech disclaimers

- I personally deployed our instance to ECS by building the docker image in ECR and then running it in ECS. appears to be very cost effective (1 month - 5 bucks!).
- participants are in-memory so if the bot goes down you will need to get everyone back online. Setting up a DB for this was annoying and not worth the extra cost or investment.
- this was built with my friends public discord in mind so if it doesnt work its probably because you dont use our custom emotes, you can look in the code and change them as you wish.
- shuffle function could be improved but i really don't care, it works good enough

## Needed env variables

you will need to create a `.env` file with `SECRET_SAUL_TOKEN` in the file. `SECRET_SAUL_TOKEN` is your discord bot secret.

## How to run

- Docker
    - build the image
  - run the container
  - Can't figure it out? look it up loser :P

- Non-Docker
  - `npm i`
  - `node secret-santa.js`

## Licensing

Do what you want with it i don't care. i will not take feature requests if they are retarded, but feel free to create issues ill likely respond. Good luck!
